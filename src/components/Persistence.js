import React, { Component } from "react";
import axios from "axios";
// const url = "https://api.jsonbin.io/b/613e722f4a82881d6c4dc56e";
const url = "https://x8ki-letl-twmt.n7.xano.io/api:MsFRHJxd/animals";

class Persistence extends Component {
  state = {
    data: [],
    form: {
      id: "",
      name: "",
    },
  };

  peticionGet = () => {
    axios
      .get(url)
      .then((response) => {
        this.setState({ data: response.data });
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  peticionPost = async () => {
    delete this.state.form.id;
    await axios
      .post(url, this.state.form)
      .then((response) => {
        this.peticionGet();
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  // peticionDelete=()=>{
  //   axios.delete(url+this.state.form.id).then(response=>{
  //     this.peticionGet();
  //   })
  // }

  deleteUser = async (userId) => {
    const response = window.confirm("are you sure you want to delete it?");
    if (response) {
      await axios.delete(
        "https://x8ki-letl-twmt.n7.xano.io/api:MsFRHJxd/animals/" + userId
      );
      this.peticionGet();
    }
  };

  seleccionaranimal = (animal) => {
    this.setState({
      form: {
        id: animal.id,
        name: animal.name,
      },
    });
  };

  handleChange = async (e) => {
    e.persist();
    await this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
    console.log("mensahe", this.state.form);
  };

  componentDidMount() {
    this.peticionGet();
  }
  render() {
    const { form } = this.state;
    return (
      <div>
        <div className="form-position">
          <input
            type="text"
            name="name"
            id="name"
            placeholder="add animal"
            onChange={this.handleChange}
            value={form ? form.name : ""}
          />
          <button onClick={() => this.peticionPost()}>+</button>

          {/* <pre>
          {datas.map((data, i) => (
            <div key={data.name} className="generated-list">
              {data.name}
              <button onClick={() => this.fRemove(i)} className="x-button">
                X
              </button>
            </div>
          ))}
        </pre> */}
        </div>
        {this.state.data.map((animal) => {
          return (
            <section key={animal.id}>
              <div>
                {animal.name}
                <button onDoubleClick={() => this.deleteUser(animal.id)}>
                  X
                </button>
               
              </div>
            </section>
          );
        })}
        {/* <section>
          <div >
            <input
              className="form-control"
              type="text"
              name="name"
              id="name"
              placeholder="add animal"
              onChange={this.handleChange}
              value={form ? form.name : ""}
            />
          <button
            className="btn btn-success"
            onClick={() => this.peticionPost()}
          >
            +
          </button>
  
          </div>
        </section> */}
      </div>
    );
  }
}

export default Persistence;
