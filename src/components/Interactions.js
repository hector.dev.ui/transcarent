import React, { Component } from "react";

class Interactions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      act: 0,
      index: "",
      datas: [],
    };
  }

  componentDidMount() {
    this.refs.name.focus();
  }

  fSubmit = (e) => {
    e.preventDefault();
    console.log("try");

    let datas = this.state.datas;
    let name = this.refs.name.value;

    if (this.state.act === 0) {
      //new
      let data = {
        name,
      };
      datas.push(data);
    } else {
      //update
      let index = this.state.index;
      datas[index].name = name;
    }

    this.setState({
      datas: datas,
      act: 0,
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  };

  fRemove = (i) => {
    let datas = this.state.datas;
    datas.splice(i, 1);
    this.setState({
      datas: datas,
    });

    this.refs.myForm.reset();
    this.refs.name.focus();
  };

  render() {
    let datas = this.state.datas;
    return (
      <div className="form-position">
        <form ref="myForm" className="myForm">
          <input
            type="text"
            ref="name"
            placeholder="add animal"
            className="formField"
          />
          <button onClick={(e) => this.fSubmit(e)}>Add </button>
        </form>
        <pre>
          {datas.map((data, i) => (
            <div key={data.name} className="generated-list">
              {data.name}
              <button onClick={() => this.fRemove(i)} className="x-button">
                X
              </button>
            </div>
          ))}
        </pre>
      </div>
    );
  }
}
export default Interactions;
