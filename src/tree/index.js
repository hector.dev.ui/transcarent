import "./index.css";
import PostDat from "./data.json";
import Interactions from "../components/Interactions";
import Persistence from "../components/Persistence";

export default function Tree() {
  return (
    <div className="tree">
      <div className="title">🎨First, layout and style</div>
      <p>1 root</p>
      <p>1.1 ant</p>
      <p>bear</p>
      <p>cat</p>
      <p>dog</p>
      <p>1.2.2.1 elephant</p>
      <p>frog</p>
      {/* ////////////////////// */}
      <p>root</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;ant</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;bear</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cat</p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dog</p>
      <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elephant
      </p>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;frog</p>
      <section className="same">
        <div>
          <p>root</p>
          <p>ant</p>
          <p>bear</p>
          <p>cat</p>
          <p>dog</p>
          <p>elephant</p>
          <p>frog</p>
        </div>
        {/* ////////////////////// */}
        <div>
          <p>root</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;ant</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;bear</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cat</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dog</p>
          <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;elephant
          </p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;frog</p>
        </div>
      </section>
      <section style={{ marginTop: "30px" }}>
        <div className="title">🔢Next, with data</div>
        <section className="data">
          <div>
            <p>root</p>
            {PostDat.animals.map(function (animals) {
              return <p key={animals.id}>{animals.name}</p>;
            })}
          </div>
          <div>
            <p>root</p>
            {PostDat.animals.reverse().map(function (animals) {
              return <p key={animals.name}>{animals.name}</p>;
            })}
          </div>
        </section>
        <div className="title">✨Let's add interactions</div>
        <section>
          <ul className="list-data">
            <p>root</p>
            {PostDat.animals.map(function (animals) {
              return <li key={animals.id} style={{display: 'flex'}}>{animals.name} <Interactions/></li>;
            })}
          </ul>
        </section>
        <section>
        <div className="title">💾And persistence!</div>
        <Persistence/>
        </section>
      </section>
    </div>
  );
}
